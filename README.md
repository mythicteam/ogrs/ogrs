# Open Gaming Reference Standard

OGRS is a proposed standard format for developing, publishing, customizing and playing tabletop games in a digital medium.  It aims to enable various tools for creating and tweaking rules systems, making new content for those systems, and presenting all of the above to players as documents, or as playable games within a program.
